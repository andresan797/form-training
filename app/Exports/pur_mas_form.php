<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use DB;
use Carbon\Carbon;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
class pur_mas_form implements FromView,ShouldAutoSize
{
   
    function __construct() {
          
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $datareport =   db::table('formtraining')->get();
            
            
        $mytime = date('Y-m-d H:i:s');
        return view('purchasing.master.reportexcel2', [
            'report' => $datareport, 
        ]);
    }
    
}
