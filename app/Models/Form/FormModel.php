<?php

namespace App\Models\Form;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormModel extends Model
{
    use HasFactory;
    protected $table = 'formtraining';
    protected $guarded = ['id'];
    protected $fillable = ['id','email','password','created_at','updated_at'];
    
    public $timestamps = true;

    
}
