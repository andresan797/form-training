<?php

namespace App\Http\Controllers\form;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Response;
use PDF;
use DataTables;
use Excel;
use Validator;
use Auth;
// use QrCode;
use Carbon\Carbon;
use App\Models\Form\FormModel;
use App\Exports\pur_mas_form;
class formController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   //table

        return view('purchasing.master.form3');
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        DB::beginTransaction();
        try{
            $messages = [
                // 'no_pembayaran.required' => 'Field no pembayaran  harus dipilih',
                'email.required' => 'Field Email  harus diisi',
                'password.required' => 'Field Password  harus diisi',
                
          
               
            ];
            $validator = Validator::make($request->all(), [
                'email' =>'required' ,
                'password' => 'required',
               
          
               
            ], $messages);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            } else {
                date_default_timezone_set('Asia/Jakarta');
               
                //tanpa modle
                // $input = db::table('formtraining')->insert([
                //     'email' => $request->email,
                //     'password' => $request->password,
                   
                //     "created_at"=>date('Y-m-d H:i:s'),
                //     "updated_at"=>date('Y-m-d H:i:s'),
                // ]);
                //pake model
               // dd($request->all());
                if($request->id==null||$request->id==""){
                    $input = FormModel::create([
                        'email' => $request->email,
                        'password' => $request->password,
                       
                        "created_at"=>date('Y-m-d H:i:s'),
                        "updated_at"=>date('Y-m-d H:i:s'),
                    ]);
                }else{
                    $input = FormModel::whereRaw("id='$request->id'")
                    
                    ->update([
                        'email' => $request->email,
                        'password' => $request->password,
                        "created_at"=>date('Y-m-d H:i:s'),
                        "updated_at"=>date('Y-m-d H:i:s'),
                    ]);

                    // $input = db::table("formtraining")
                    // ->whereRaw("id='$request->id'")
               
                    // ->update([
                    //     'email' => $request->email,
                    //     'password' => $request->password,
                    //     "created_at"=>date('Y-m-d H:i:s'),
                    //     "updated_at"=>date('Y-m-d H:i:s'),
                    // ]);

                }
              
                $message['status']="Berhasil Simpan";
                DB::commit();
                return Response()->json([
                    $message,
                    'success'=>'True'
                ]);
            }
        }catch(\Exception $e){
            $success = "Gagal";
            DB::rollback();
            return Response::json(['errors' => "Backend Error Pada Line" . $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        DB::beginTransaction();
        try{
            date_default_timezone_set('Asia/Jakarta');
               
            $input = FormModel::whereRaw("id='$id'")->delete();

           

              
            $message['status']="Berhasil Simpan";
            DB::commit();
            return Response()->json([
                $message,
                'success'=>'True'
            ]);
        }catch(\Exception $e){
            $success = "Gagal";
            DB::rollback();
            return Response::json(['errors' => "Backend Error Pada Line" . $e->getMessage()]);
        }
    }

    public function getdata($id){
        $data = db::table('formtraining')->whereRaw("email like '%$id%'")->first();

        if(!empty($data)){
            return response()->json([
                'data' => $data,
                "status"=>"true"
            ]);
        }else{
            return response()->json([
                'data' => [],
                "status"=>'false'
            ]);
        }
       
    }

    public function api(){
        $data =  db::table('formtraining')->get();

       
      //  dd($data);
        return DataTables::of($data)
        ->addIndexColumn()
        ->addColumn('action', function($data){
            $value=$data->id."_".$data->email."_".$data->password;
           
            $btn = '';
            $btn = $btn. '<a onclick="edit(\''.$value.'\')" class="btn btn-icon btn-icon btn-primary btn-sm mr-1 mb-1" style="color :white;background-color:#558B2F !Important;width:100px"><span class="fa fa-book">&nbsp;Edit</span> </a>&nbsp;<br>';
            return $btn;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function apiexcell(){
        // dd("cok");
        date_default_timezone_set('Asia/Jakarta');
      
        return Excel::download(new pur_mas_form(), 'reportlogin2'.' '. date("Y-m-d").'.xlsx');
    }


    public function apipdf(){


        $datareport =   db::table('formtraining')
      
        ->get();
     
        $pdf = PDF::loadview('purchasing.master.reportexcel',compact('datareport'));
        $pdf->setPaper('A4', 'potrait');

        return $pdf->stream();
       // return $pdf->download();
    }
}
