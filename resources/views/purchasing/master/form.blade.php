@extends('layout.layout')
@section('content')
  @section('css')
    <link rel="stylesheet" href="css/styleku.min.css">
  @endsection
<div class="container">
  <h2>FORM</h2>
  <form>
    <div class="form-group">
      <label for="nama">Nama:</label>
      <input type="text" class="form-control" id="nama" placeholder="Masukkan nama" name="nama">
    </div>
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Masukkan email" name="email">
    </div>
    <div class="form-group">
      <label for="pesan">Pesan:</label>
      <textarea class="form-control" rows="5" id="pesan" placeholder="Masukkan pesan" name="pesan"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
 @endsection

@section('script')
<script src="js/functionform/function.js"></script>
<script>
    $(document).ready(function() {

             alert("HALO");
    })
</script>
@endsection