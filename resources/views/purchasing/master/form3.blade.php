@extends('layout.layout')
@section('content')
@section('css')
@endsection
    <div  class="container">
        <h2>FORM INPUT MASTER</h2>
        
        <form id="formku">
            {{ csrf_field() }}
            <div class="form-group row">
                <input  type="text" class="form-control" id="id" name="id" placeholder="id">
                <div class="col-sm-2">
                    <label for="email" class=" col-form-label">Email</label>
                </div>
               
                <div class="col-sm-4">
                  <input  type="email" class="form-control" id="email" name="email" placeholder="Email">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2">
                    <label for="password" class=" col-form-label">Password</label>
                </div>
               
                <div class="col-sm-4">
                  <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
            </div>

        </form>
        <button onclick="inputclick()" type="button" class="btn  btn-primary">INPUT</button>&nbsp;
        <button onclick="destroy()" type="button" class="btn  btn-danger">DELETE</button>&nbsp;
        <a id="pdf" class="btn btn-success" href="{{ route('pdf.print') }}">PRINT pdf</a>
        <a id="excell" class="btn btn-success" href="{{ route('excell.print') }}">PRINT excell</a>
                               
        <br>
        <br>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="indextable" class="table add-rows">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Email</th>
                                            <th>Password</th>
                                        
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Email</th>
                                            <th>Password</th>
                                        
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
<script>
    
    $(document).ready(function() {

        //datatable
        var table = $('#indextable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '<?= url("pur_mas_form/api") ?>',
               
                columns: [
                    {data: 'DT_RowIndex',orderable: false,searchable: false},
                    {data: 'email', name: 'email'},
                    {data: 'password', name: 'password'},
                  
	                 {data: 'action', name: 'action', orderable: false, searchable: false},
	            ]
        });

        //searchdata by email
        $("#email").on('keyup', function (e) {
            if (e.key === 'Enter' || e.keyCode === 13) {
                var sup_code = $(this).val();
                $.ajax({
                    url:  "<?php echo url('pur_mas_form/getdata/')?>/"+sup_code,
                    type: "get",
                    dataType: "JSON",
                    success: function (response) {
                        if(response.status=="Gagal"){
                            Swal.fire({
                                text: 'AKUN TIDAK DITEMUKAN',
                                icon: 'warning',
                                confirmButtonText: 'OK'
                            })
                        }else{
                            var dataku=response.data;
                            console.log(dataku);
                            console.log("cokcok");
                            $("#id").val("");
                            $("#id").val(response.data.id);
                            $("#email").val(response.data.email);
                            $("#password").val(response.data.password);
                        
                        }
                    
                    
                    }         
                }); 
            }
        });
    })
   
   function edit(params){
        var data=params.split("_");
        console.log(data);
        $("#id").val("");
        $("#id").val(data[0]);
        $("#email").val(data[1]);
        $("#password").val(data[2]);
   }
    function inputclick(){
        var formData = $("#formku").serialize();
      
        $.ajax({
            url: "<?php echo url('pur_mas_form/store'); ?>",
            type: "POST",
            data: formData,
            dataType: "JSON",
            // cache:false,
            // contentType: false,
            // processData: false,
            success: function(data) {
                if (data.success == "True") {
                        Swal.fire({
                            text: 'Berhasil Simpan',
                            icon: 'success',
                            confirmButtonText: `Yes`,
                            customClass: {
                                confirmButton: 'order-2',
                                denyButton: 'order-3',
                            }
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = "{{ route('pur_mas_form') }}";
                            } else if (result.isDenied) {
                                return false;
                            }
                        });
                }else if (data.Status=="GAGAL") {
                        Swal.fire({
                            html: data.Message,
                            icon: 'warning',
                            confirmButtonText: 'OK'
                        })
                    } 
                else if (data.errors) {
                        var values = '';
                        for (var i = 0; i < data.errors.length; i++) {
                            var replace = data.errors[i].replaceAll(" diisi", " diisi !!!! <br>");
                            data.errors[i] = replace;
                        }
                        Swal.fire({
                            html: data.errors,
                            icon: 'warning',
                            confirmButtonText: 'OK'
                        })
                }
            },
        });
    }


    function destroy(){
        
        var id=$("#id").val();
        if(id==""){
            Swal.fire({
                text: "DATA KOSONG tidak bisa delete",
                icon: 'warning',
                confirmButtonText: 'OK'
            })
        }else{
            $.ajax({
            url: "<?php echo url('pur_mas_form/destroy/'); ?>"+"/"+id,
            type: "get",
            dataType: "JSON",
            // cache:false,
            // contentType: false,
            // processData: false,
            success: function(data) {
                if (data.success == "True") {
                        Swal.fire({
                            text: 'Berhasil delete data',
                            icon: 'success',
                            confirmButtonText: `Yes`,
                            customClass: {
                                confirmButton: 'order-2',
                                denyButton: 'order-3',
                            }
                        }).then((result) => {
                            if (result.isConfirmed) {
                              
                            } else if (result.isDenied) {
                                return false;
                            }
                        });
                }else if (data.Status=="GAGAL") {
                        Swal.fire({
                            html: data.Message,
                            icon: 'warning',
                            confirmButtonText: 'OK'
                        })
                    } 
                else if (data.errors) {
                        var values = '';
                        for (var i = 0; i < data.errors.length; i++) {
                            var replace = data.errors[i].replaceAll(" diisi", " diisi !!!! <br>");
                            data.errors[i] = replace;
                        }
                        Swal.fire({
                            html: data.errors,
                            icon: 'warning',
                            confirmButtonText: 'OK'
                        })
                }
            },
        });
        }
       
    }

</script>
@endsection