<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\form\formController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//route form
Route::get('/pur_mas_form', [formController::class, 'create'])->name('pur_mas_form');
//route insert
Route::POST('/pur_mas_form/store', [formController::class, 'store']);
//route get edit data
// Route::get('/pur_mas_form/edit/{id}', [formController::class, 'edit']);
//update
Route::POST('/pur_mas_form/update', [formController::class, 'update']);
//route destroy daya
Route::get('/pur_mas_form/destroy/{id}', [formController::class, 'destroy']);

Route::get('/pur_mas_form/getdata/{id}', [formController::class, 'getdata']);
Route::get('/pur_mas_form/api', [formController::class, 'api']);

Route::get('/pur_mas_form/apipdf', [formController::class, 'apipdf'])->name('pdf.print');
Route::get('/pur_mas_form/apiexcell', [formController::class, 'apiexcell'])->name('excell.print');;
